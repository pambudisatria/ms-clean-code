ARG versionGo=
ARG versionFluentd=
FROM image-repo.bri.co.id/nds/ubi8-go:${versionGo} as builder

#set environment--http_proxy, open container's connection to internet for downloading wget and vim
ENV http_proxy http://proxy2.bri.co.id:1707
ENV https_proxy http://proxy2.bri.co.id:1707

WORKDIR /builder

COPY . .

RUN go mod download && make build 

## Distribution
FROM image-repo.bri.co.id/nds/ubi8-fluentd:${versionFluentd}

WORKDIR /app

RUN mkdir /app/logs
	
COPY --from=builder /builder/main /builder/wrapper_script.sh /app/
COPY --from=builder /builder/fluent.conf /conf/
COPY --from=builder /builder/internal /app/internal

RUN chmod +x wrapper_script.sh \
	&& chmod +x /conf/fluent.conf \
	&& chmod -R 770 /app/logs	

#set environment--http_proxy, open container's connection to internet for downloading wget and vim
ENV http_proxy ""
ENV https_proxy ""

EXPOSE 7542

CMD /bin/bash -c ./wrapper_script.sh
