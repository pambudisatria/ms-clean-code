# **NDS PROJECT - Clean Code Micro Service Version 1**

# About

This is the main repository for Clean Code Micro service version 1. 

# Prerequisites

- [Go](https://golang.org/dl/) version 1.13+
- [Protocol Buffer](https://developers.google.com/protocol-buffers/docs/downloads). For Mac User, simply install using `brew install protobuf` from the terminal 
- protoc-gen-go : `go get -u github.com/golang/protobuf/protoc-gen-go@v1.3.5`. Read from [here](https://grpc.io/docs/quickstart/go/) for further information.

# Unit Testing

- Generate all proto files: `make protoc`
- Run the test: `make test`

# Build the Service

`make build`

