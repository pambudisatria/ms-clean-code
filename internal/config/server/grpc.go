package server

// ServerList :
type ServerList struct {
	Grpc Server // main gRPC config for this service

	//Add other gRPC client service configuration here...
}

// Server :
type Server struct {
	TLS     bool   `yaml:"tls"`
	Host    string `yaml:"host"`
	Port    int    `yaml:"port"`
	Timeout int    `yaml:"timeout"`
}
