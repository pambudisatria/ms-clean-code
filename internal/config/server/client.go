package server

// ClientList object client list
type ClientList struct {
	Rest Rest
}

// Rest object rest
type Rest struct {
	Fourgen Client `yaml:"fourgen"`
}

// Client object client
type Client struct {
	URL          string `yaml:"url"`
	Endpoint     string `yaml:"endpoint"`
	ClientID     string `yaml:"clientID"`
	ClientSecret string `yaml:"clientSecret"`
	ChannelID    string `yaml:"channelID"`
}
