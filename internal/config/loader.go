package config

import (
	"fmt"
	"os"
	"strings"

	"bitbucket.org/ms-clean-code/internal/config/db"
	"bitbucket.org/ms-clean-code/internal/config/logging"
	"bitbucket.org/ms-clean-code/internal/config/server"

	"github.com/spf13/viper"
)

// Config ...
type Config struct {
	Server   server.ServerList
	Database db.DatabaseList
	Client   server.ClientList
	Logger   logging.LoggerConfig
	EsbDate  string `yaml:"esbDate"`
}

var cfg Config

func init() {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	fmt.Println(dir)

	viper.AddConfigPath(dir + "/internal/config/server")
	viper.SetConfigType("yaml")
	viper.SetConfigName("grpc.yml")
	err = viper.MergeInConfig()
	if err != nil {
		panic(fmt.Errorf("Cannot load server config: %v", err))
	}

	viper.AddConfigPath(dir + "/server")
	viper.SetConfigName("client.yml")
	err = viper.MergeInConfig()
	if err != nil {
		panic(fmt.Errorf("Cannot load server client config: %v", err))
	}

	viper.AddConfigPath(dir + "/internal/config/logging")
	viper.SetConfigType("yaml")
	viper.SetConfigName("logger.yml")
	err = viper.MergeInConfig()
	if err != nil {
		panic(fmt.Errorf("Cannot load server config: %v", err))
	}

	viper.AddConfigPath(dir + "/internal/config/db")
	viper.SetConfigName("mysql.yml")
	err = viper.MergeInConfig()
	if err != nil {
		panic(fmt.Errorf("Cannot read database config: %v", err))
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	viper.Unmarshal(&cfg)
}

// GetConfig ...
func GetConfig() *Config {
	return &cfg
}
