package main

import (
	"log"

	cfg "bitbucket.org/ms-clean-code/internal/config"
	grpcAPI "bitbucket.org/ms-clean-code/pkg/infrastructure/grpc/api"
	"bitbucket.org/ms-clean-code/pkg/shared/logger"
)

func main() {
	config := cfg.GetConfig()

	logConfig := logger.Configuration{
		EnableConsole:     config.Logger.Console.Enable,
		ConsoleJSONFormat: config.Logger.Console.JSON,
		ConsoleLevel:      config.Logger.Console.Level,
		EnableFile:        config.Logger.File.Enable,
		FileJSONFormat:    config.Logger.File.JSON,
		FileLevel:         config.Logger.File.Level,
		FileLocation:      config.Logger.File.Path,
	}

	if err := logger.NewLogger(logConfig, logger.InstanceZapLogger); err != nil {
		log.Fatalf("Could not instantiate log %v", err)
	}

	grpcAPI.RunServer()
}
