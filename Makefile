.PHONY: clean protoc test grpc build

clean:
	@echo "--- Cleanup all build and generated files ---"
	@rm -vf pkg/infrastructure/grpc/proto/build/*.pb.go
	@rm -vf pkg/infrastructure/grpc/proto/validator/*.pb.go
	@rm -vf ./main
	@rm -vf ./ms-clean-code

protoc: clean
	@echo "--- Preparing proto output directories ---"
	@mkdir -p pkg/infrastructure/grpc/proto/build
	@mkdir -p pkg/infrastructure/grpc/proto/validator
	@echo "--- Compiling all proto files ---"
	@cd ./pkg/shared/proto/validator && protoc -I. --go_out=plugins=grpc:../../../infrastructure/grpc/proto/validator *.proto	
	# @cd ./pkg/shared/proto/_yourfolder_ && protoc -I. -I ../validator --go_out=plugins=grpc:../../../infrastructure/grpc/proto/build --govalidators_out=../../../infrastructure/grpc/proto/build *.proto

test: protoc
	@echo "--- Running test files, this could take few moments ---"
	@cd pkg && go test -v -cover ./...

grpc:
	@echo "--- running gRPC server in dev mode ---"
	@go run cmd/server/grpc/main.go

setup:
	@cp internal/config/example/mysql.yml.example internal/config/db/mysql.yml
	@cp internal/config/example/client.yml.example internal/config/server/client.yml
	@cp internal/config/example/grpc.yml.example internal/config/server/grpc.yml
	@cp internal/config/example/logger.yml.example internal/config/logging/logger.yml

build: setup protoc
	@echo "--- Building binary file ---"
	@go build -o ./main cmd/server/grpc/main.go
