module bitbucket.org/ms-clean-code

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-resty/resty/v2 v2.3.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.3.3
	github.com/google/uuid v1.1.1
	github.com/jinzhu/configor v1.2.0
	github.com/jinzhu/gorm v1.9.15
	github.com/mitchellh/mapstructure v1.1.2
	github.com/mwitkow/go-proto-validators v0.3.0
	github.com/sarulabs/di v2.0.0+incompatible
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.15.0
	google.golang.org/grpc v1.30.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
