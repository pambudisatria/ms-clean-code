# /pkg/usecase

Folder to save your interactor grouping based on context in one folder.
In one folder contains 3 file with suffix: ```_interactor```.go, ```_ioport```.go and ```_test```.go

*example:*

- product
    
	- product_interactor.go
    
	- product_ioport.go
    
	- product_test.go

## 1. _interactor.go
Your interactor file.

*example:*
```go
package usecase

import (
    "fmt"
    
    "/pkg/domain/repository"

    "github.com/mitchellh/mapstructure"
)

type Usecase struct {
	repo repository.Repository
	out  OutputPort
}

type Request struct {
	ID   int64
}

type Response struct {
	ID   int64
	Name string
}

func NewUsecase(r repository.Repository, o OutputPort) *Usecase {
	return &Usecase{
		repo: r,
		out:  o,
	}
}

func (uc *Usecase) FunctionName(data interface{}) (interface{}, error) {
	if data == nil {
		return nil, fmt.Errorf("Some Error")
	}

	request := data.(*Request)
	out, err := uc.repo.FunctionName(request)
	if err != nil {
		return nil, err
	}

	var response *Response
	err = mapstructure.Decode(out, &out)
	if err != nil {
		return nil, fmt.Errorf("Some Error")
	}

	return uc.out.GetFunctionNameResponse(response)
}

```

## 2. _ioport.go
Your interactor file.

*example:*
```go
package usecase

type InputPort interface {
	FunctionName(interface{}) (interface{}, error)
}

type OutputPort interface {
	GetFunctionNameResponse(interface{}) (interface{}, error)
}
```

## 3. _test.go
Your unit test file. Give at least minimal two scenarios (positive and negative).