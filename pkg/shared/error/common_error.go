package error

// Error Const
const (
	InvalidArgument   = "INVALID_ARGUMENT"
	InvalidRestClient = "INVALID_REST_CLIENT"

	// TIMEOUT_SERVICE indicate to trigger timeout transaction
	TimeoutService = "TIMEOUT_SERVICE"

	// INVALID_RESPONSE indicate response not match with response structure
	InvalidResponse = "INVALID_RESPONSE"
)
