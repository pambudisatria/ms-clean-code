# /pkg/shared/mock

## 1. adapter
Mocking for output port on usecase. Naming your file based on context with suffix ```_adapter```.go.

*example:*
```go
package adapter

import (
	"github.com/stretchr/testify/mock"
)

type MockAdapter struct {
	mock.Mock
}

func (m *MockAdapter) GetFunctionNameResponse(data interface{}) (interface{}, error) {
	call := m.Called(data)
	res := call.Get(0)
	if res == nil {
		return nil, call.Error(1)
	}
	return res, call.Error(1)
}
```

## 2. repository
Mocking for input port on usecase. Naming your file based on context with suffix ```_repository```.go.

*example:*
```go
package repository

import (
	"github.com/stretchr/testify/mock"
)

type MockRepository struct {
	mock.Mock
}

func (m *MockRepository) FunctionName(in interface{}) (interface{}, error) {
	call := m.Called(in)
	res := call.Get(0)
	if res == nil {
		return nil, call.Error(1)
	}
	return res, call.Error(1)
}
```