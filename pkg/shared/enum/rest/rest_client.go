package rest

// RestClient for instance of rest client
type RestClient string

const (
	// Resty for instance resty
	Resty RestClient = "resty"
)
