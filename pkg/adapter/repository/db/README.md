# /pkg/adapter/repository/db

Client helper DB

*example:*
```go
package db

import (
	"github.com/jinzhu/gorm"
	"github.com/mitchellh/mapstructure"

	dbConf "/internal/config/db"
	"/pkg/adapter/db"
	"/pkg/adapter/db/model"
	uc "/pkg/usecase"
)

type DataHandler struct {
	repo db.DbDriver
	db   *gorm.DB
}

func NewDataHandler(databases dbConf.DatabaseList) *DataHandler {
	driver, _ := db.NewInstanceDb(databases.Nds.Mysql)
	return &DataHandler{
		repo: driver,
		db:   driver.Db().(*gorm.DB),
	}
}

func (r *DataHandler) FunctionName(item interface{}) (interface{}, error) {
    var modelYourTableName model.YourTableName
    request := item.(*uc.Request)

	err := r.db.Debug().Where("id = ?", request.ID).Find(&modelYourTableName).Error
	if err != nil {
		return nil, err
	}

	var response *uc.Response
	err = mapstructure.Decode(modelYourTableName, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
```