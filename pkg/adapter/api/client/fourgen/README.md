# /pkg/adapter/api/client/fourgen

Client helper API to ESB

*example:*
```go
package fourgen

import (
	"fmt"
	"strconv"
	"strings"

	"/pkg/domain/entity"
	"/pkg/shared/util"
	uc "/pkg/usecase"
)

type ClientHelper struct {
	client *FourgenClient
}

type Request struct {
	PriorityAccountNumber string `json:"DYPACN"`
	PriorityAccountType   string `json:"DYPACT"`
	RelationshipCode      string `json:"DYRELT"`
	SequenceNumber        string `json:"DYSEQN"`
}

func NewClientHelper(c *FourgenClient) *ClientHelper {
	return &ClientHelper{
		client: c,
	}
}

func (p *ClientHelper) FunctionName(item interface{}) (interface{}, error) {
	var data = item.(*uc.Request)
	var middlewareHeader = DMiddleWareHeader{
		Trancode: "9999",
	}
	var mbaseHeader = DMbaseHeader{
		Cif:                 "XXXXXX",
		TransactionCode:     "9999",
		MoreRecordIndicator: "N",
		NoOfRecord:          "001",
		AccountType:         data.PriorityAccountType,
		ActionCode:          "I",
		Account:             data.PriorityAccountNumber,
		SearchMethod:        "F",
	}
	var request = MbaseRequest{
		DSockerHeader:     &DSocketHeader{},
		DMiddleWareHeader: &middlewareHeader,
		DMbaseHeader:      &mbaseHeader,
		DMbaseMessage: &InquiryDetailA2ARelRequest{
			PriorityAccountNumber: data.PriorityAccountNumber,
			PriorityAccountType:   data.PriorityAccountType,
			RelationshipCode:      data.RelationshipCode,
			SequenceNumber:        data.SequenceNumber,
		},
		ApplicationName:    "INDS",
		Branchcode:         data.BranchCode,
		TellerID:           data.UserID,
		SupervisorID:       "",
		OriginalJournalSeq: "",
		Trancode:           "9999",
	}

	response := &MbaseResponse{}
	fourgenClient, _ := p.client.SetRequest(request)
	err := fourgenClient.Post(response)

	if err != nil {
		return nil, fmt.Errorf("Some Error")
	}

	if response.StatusCode != "0001" {
		return nil, fmt.Errorf("Some Error")
	}

	return &en.Response{
		JournalSequence:         response.JournalSeq,
		StatusCode:              response.StatusCode,
		StatusDescription:       response.StatusDesc,
		PriorityAccountNumber:   response.MsgMBMessage[0],
		PriorityAccountType:     response.MsgMBMessage[1],
		RelationshipCode:        response.MsgMBMessage[2],
		SequenceNumber:          response.MsgMBMessage[3],
	}, err
}
```