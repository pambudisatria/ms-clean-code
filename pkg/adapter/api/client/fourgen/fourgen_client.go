package fourgen

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"strings"

	"bitbucket.org/ms-clean-code/pkg/adapter/api/client/rest"
	enumRest "bitbucket.org/ms-clean-code/pkg/shared/enum/rest"

	cfgClient "bitbucket.org/ms-clean-code/internal/config/server"

	"github.com/go-resty/resty/v2"
)

type (
	// DSocketHeader :
	DSocketHeader struct{}

	// DMiddleWareHeader :
	DMiddleWareHeader struct {
		Trancode string `json:"TRANCODE"`
	}

	// DMbaseHeader :
	DMbaseHeader struct {
		Cif                 string `json:"CIF"`
		TransactionCode     string `json:"TRANSACTIONCODE"`
		MoreRecordIndicator string `json:"MORERECORDINDICATOR"`
		NoOfRecord          string `json:"NOOFRECORD"`
		AccountType         string `json:"ACCOUNTTYPE"`
		ActionCode          string `json:"ACTIONCODE"`
		Account             string `json:"ACCOUNT"`
		SearchMethod        string `json:"SEARCHMETHOD"`
	}

	// MbaseRequest :
	MbaseRequest struct {
		DSockerHeader      *DSocketHeader     `json:"dSocketHeader"`
		DMiddleWareHeader  *DMiddleWareHeader `json:"dMiddleWareHeader"`
		DMbaseHeader       *DMbaseHeader      `json:"dMbaseHeader"`
		DMbaseMessage      interface{}        `json:"dMbaseMessage"`
		ApplicationName    string             `json:"applicationname"`
		Branchcode         string             `json:"branchcode"`
		TellerID           string             `json:"tellerid"`
		SupervisorID       string             `json:"supervisorid"`
		OriginalJournalSeq string             `json:"origjournalseq"`
		Trancode           string             `json:"trancode"`
	}
	// MbaseResponse :
	MbaseResponse struct {
		JournalSeq    string   `json:"journalseq"`
		StatusCode    string   `json:"statuscode"`
		StatusDesc    string   `json:"statusdesc"`
		MsgSockHeader []string `json:"msgsockheader"`
		MsgMWHeader   []string `json:"msgmwheader"`
		MsgMBHeader   []string `json:"msgmbheader"`
		MsgMBMessage  []string `json:"msgmbmessage"`
	}
)

// Client ...
type Client struct {
	endpoint string
	headers  map[string]string
	body     interface{}
	handler  rest.ClientRepository
}

// NewClientWithConfig :
func NewClientWithConfig(config cfgClient.Client) (*Client, error) {
	client, _ := rest.NewRestClient(enumRest.Resty)
	return &Client{
		endpoint: config.URL + config.Endpoint,
		headers: map[string]string{
			"Content-Type": "application/x-www-form-urlencoded",
		},
		handler: client,
	}, nil
}

// SetRequest set request
func (e *Client) SetRequest(request interface{}) (*Client, error) {
	// TODO: need catch when json marshal error
	reqByte, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req := "jsonReq=" + string(reqByte)
	fmt.Printf("Request %v\n", req)
	e.body = req
	return e, nil
}

// Post Initiate ESB Post command
func (e *Client) Post(out interface{}) error {
	fmt.Printf("Endpoint %v\n", e.endpoint)
	fmt.Printf("Headers %v\n", e.headers)

	resp, err := e.handler.Post(e.endpoint, e.headers, e.body)
	response := resp.(*resty.Response)
	if err != nil {
		return err
	}
	var body = response.Body()
	//fmt.Printf("Body %v\n", string(body))

	newBody := strings.ReplaceAll(string(body), "<>", "")
	fmt.Printf("Body %v\n", string(newBody))

	xmlParsed := make([]string, 2)
	if err := xml.Unmarshal([]byte(newBody), &xmlParsed); err != nil {
		return err
	}

	if xmlParsed[2] == "null" {
		return fmt.Errorf("null response from briinterface")
	}

	if err = json.Unmarshal([]byte(xmlParsed[2]), out); err != nil {
		return err
	}

	return nil
}

// GetClient get client resty
func (e *Client) GetClient() interface{} {
	cli := e.handler.GetClient().(*resty.Client)
	return cli.GetClient()
}
