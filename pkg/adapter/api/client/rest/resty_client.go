package rest

import (
	"net/http"
	"time"

	"github.com/go-resty/resty/v2"
)

// RestyClient ...
type RestyClient struct {
	client  *resty.Client
	headers map[string]string
}

// NewRestyClient new object resty client
func NewRestyClient() *RestyClient {
	return &RestyClient{
		client: resty.New(),
	}
}

// Post send using post method
func (c *RestyClient) Post(endpoint string, headers map[string]string, body interface{}) (interface{}, error) {
	c.headers = headers
	data, err := c.client.SetTimeout(30 * time.Second).SetPreRequestHook(c.beforeRequest).R().SetBody(body).Post(endpoint)
	return data, err
}

// GetClient get client of resty
func (c *RestyClient) GetClient() interface{} {
	return c.client
}

func (c *RestyClient) beforeRequest(r *resty.Client, h *http.Request) error {
	for k, v := range c.headers {
		h.Header[k] = []string{v}
	}
	return nil
}
