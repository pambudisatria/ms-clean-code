package rest

import (
	"fmt"

	enumRest "bitbucket.org/ms-clean-code/pkg/shared/enum/rest"
	errorCode "bitbucket.org/ms-clean-code/pkg/shared/error"
)

// ClientRepository rest client repository
type ClientRepository interface {
	GetClient() interface{}
	Post(endpoint string, headers map[string]string, body interface{}) (interface{}, error)
}

// NewRestClient new rest client
func NewRestClient(restType enumRest.RestClient) (ClientRepository, error) {
	switch restType {
	case enumRest.Resty:
		return NewRestyClient(), nil
	default:
		return nil, fmt.Errorf(errorCode.InvalidRestClient)
	}
}
