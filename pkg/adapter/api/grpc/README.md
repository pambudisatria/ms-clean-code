# /pkg/adapter/api/grpc

## 1. dto
Builder to generate response. 
Save your file inside *dto* folder.
Naming your file based on context with suffix ```_builder```.go.

*example:*
```go
package dto

import (
	"fmt"

	proto "/pkg/infrastructure/grpc/proto/build"
	uc "/pkg/usecase"

	"github.com/mitchellh/mapstructure"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Builder struct {}

func (*Builder) GetFunctionNameResponse(in interface{}) (interface{}, error) {
	var out *proto.Response
	err := mapstructure.Decode(in, &out)
	if err != nil {
		return nil, status.Error(codes.DataLoss, fmt.Sprintf("Server internal error, Invalid data response format"))
	}
	return out, nil
}
```

## 2. service
Implementation GRPC service.
Naming your file based on context with suffix ```_service```.go.

*example:*
```go
package grpc

import (
	"context"
	"fmt"

	proto "/pkg/infrastructure/grpc/proto/build"
	uc "/pkg/usecase"

	"github.com/mitchellh/mapstructure"
)

type Service struct {
	repo uc.InputPort
}

func NewService(r uc.InputPort) *Service {
	return &Service{r}
}

func (p *Service) FunctionName(ctx context.Context, in *proto.Request) (*proto.Response, error) {
	var request = &uc.Request{
		ID: in.GetID(),
	}
	data, err := p.repo.FunctionName(request)
	if err != nil {
		return nil, err
	}

	var out *proto.Response
	err = mapstructure.Decode(data, &out)
	if err != nil {
		return nil, fmt.Errorf("Some Error")
	}
	return out, nil
}
```

## 3. unit test
Your unit test file. Give at least minimal two scenarios (positive and negative).
Naming your file based on context with suffix ```_service_test```.go.