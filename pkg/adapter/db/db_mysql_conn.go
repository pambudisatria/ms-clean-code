package db

import (
	dbConf "bitbucket.org/ms-clean-code/internal/config/db"

	_ "github.com/go-sql-driver/mysql" // Initiate mysql driver
	"github.com/jinzhu/gorm"
)

// DriverMySQL object Driver MySQL
type DriverMySQL struct {
	config dbConf.Database
	db     *gorm.DB
}

// NewMySQLDriver new object SQL Driver
func NewMySQLDriver(config dbConf.Database) (DbDriver, error) {
	dbConn, err := connect(config)
	// Disable table name's pluralization, if set to true, `User`'s table name will be `user`
	dbConn.SingularTable(true)

	if err != nil {
		return nil, err
	}

	return &DriverMySQL{
		config: config,
		db:     dbConn,
	}, nil
}

func connect(config dbConf.Database) (*gorm.DB, error) {
	user := config.Username
	password := config.Password
	host := config.Host
	port := config.Port
	dbname := config.Name

	// dbConn, err = gorm.Open("mysql", "user:password@/dbname?charset=utf8&parseTime=True&loc=Local")
	dbConn, err := gorm.Open("mysql", user+":"+password+"@("+host+":"+port+")/"+dbname+"?charset=utf8&parseTime=True&loc=Local")
	// dbConn, err := sql.Open("mysql", user+":"+password+"@tcp("+host+":"+port+")/"+dbname)
	return dbConn, err
}

// Db get db instance of gorm
func (m *DriverMySQL) Db() interface{} {
	return m.db
}
