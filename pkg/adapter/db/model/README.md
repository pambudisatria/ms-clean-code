# /pkg/adapter/db/model

Folder to save your db model file grouping based on context.

*example:*
```go
package model

type YourTableName struct {
	ID   int64 `gorm:"column:id"`
	Name string `gorm:"column:name"`
}
```