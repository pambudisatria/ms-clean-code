package db

import (
	"errors"
	"log"
	"sync"

	dbConf "bitbucket.org/ms-clean-code/internal/config/db"
	"bitbucket.org/ms-clean-code/pkg/shared/enum"

	_ "github.com/go-sql-driver/mysql"
)

var (
	errorInvalidDbInstance = errors.New("Invalid db instance")
)

var atomicinz uint64
var instanceDb map[string]DbDriver = make(map[string]DbDriver)

var once sync.Once

// DbDriver is object DB
type DbDriver interface {
	Db() interface{}
	// Dml(command string, params ...string) error
	// Query(command string, params ...string) (*sql.Rows, error)
}

// NewInstanceDb is used to create a new instance DB
func NewInstanceDb(config dbConf.Database) (DbDriver, error) {
	var err error
	var dbName = config.Name

	// once.Do(func() {
	switch config.Adapter {
	case enum.MySql:
		dbConn, sqlErr := NewMySQLDriver(config)
		if sqlErr != nil {
			err = sqlErr
			// TODO: need handle if database disconnect
			log.Fatal("Disconnect Database :: " + " [" + dbName + "] " + err.Error())
		}
		instanceDb[dbName] = dbConn
		// instanceDb[dbName], _ := NewMySqlDriver(config) // --> TODO: why error ??
	default:
		err = errorInvalidDbInstance
	}
	// })

	return instanceDb[dbName], err
}
