package api

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	cfg "bitbucket.org/ms-clean-code/internal/config"
	container "bitbucket.org/ms-clean-code/pkg/shared/di"
	"bitbucket.org/ms-clean-code/pkg/shared/logger"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/google/uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type validator interface {
	Validate() error
}

// RunServer run server
func RunServer() {
	config := cfg.GetConfig()
	tls := config.Server.Grpc.TLS
	opts := []grpc.ServerOption{}

	if tls {
		serverCert := "server.crt"
		serverKey := "server.key"
		creds, err := credentials.NewServerTLSFromFile(serverCert, serverKey)
		if err != nil {
			log.Fatalln("Failed to loading certificates: ", err)
			os.Exit(1)
		}

		opts = append(opts, grpc.Creds(creds))
	}
	opts = append(opts, grpc.UnaryInterceptor(serverInterceptor))

	grpcServer := grpc.NewServer(opts...)
	ctn := container.NewContainer()

	Apply(grpcServer, ctn)

	svcHost := config.Server.Grpc.Host
	svcPort := config.Server.Grpc.Port

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", svcHost, svcPort))
		if err != nil {
			log.Fatalf("Failed to listen: %v", err)
		}

		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("Failed to start Clean Code Microservices gRPC server: %v", err)
		}
	}()

	fmt.Printf("Clean Code Microservices gRPC server is running at %s:%d\n", svcHost, svcPort)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	signal := <-c
	log.Fatalf("process killed with signal: %v\n", signal.String())
}

// Apply :
func Apply(server *grpc.Server, ctn *container.Container) {

}

func serverInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	m, err := getMetadata(ctx)
	logUUID, _ := uuid.NewRandom()
	logger.WriteLog(logger.Info, req, m, "request", "", info.FullMethod, logUUID.String(), codes.OK)

	if err != nil {
		return container.MetaData{}, status.Errorf(codes.InvalidArgument, err.Error())
	}
	if _, err := validateMetadata(info, m); err != nil {
		return nil, err
	}

	if v, ok := req.(validator); ok {
		if err := v.Validate(); err != nil {
			logger.WriteLog(logger.Error, req, m, "request", err.Error(), info.FullMethod, logUUID.String(), codes.InvalidArgument)
			return nil, status.Errorf(codes.InvalidArgument, err.Error())
		}
	}
	// Calls the handler
	res, err := handler(ctx, req)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			logger.WriteLog(logger.Error, nil, m, "response", e.Message(), info.FullMethod, logUUID.String(), e.Code())
		}
	} else {
		logger.WriteLog(logger.Info, res, m, "response", codes.OK.String(), info.FullMethod, logUUID.String(), codes.OK)
	}
	return res, err
}

func getMetadata(ctx context.Context) (container.MetaData, error) {
	var metaData container.MetaData
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return container.MetaData{}, status.Errorf(codes.InvalidArgument, "UnaryEcho: failed to get metadata")
	}

	if t, ok := md["clientname"]; ok {
		metaData.ClientName = t[0]
	}

	if t, ok := md["clientip"]; ok {
		metaData.ClientIP = t[0]
	}

	return metaData, nil
}

func validateMetadata(info *grpc.UnaryServerInfo, m container.MetaData) (container.MetaData, error) {

	if err := validation.ValidateStruct(&m,
		validation.Field(&m.ClientName, validation.Required),
		validation.Field(&m.ClientIP, validation.Required, is.IPv4),
	); err != nil {
		return container.MetaData{}, status.Errorf(codes.InvalidArgument, err.Error())
	}

	return m, nil
}
