# /pkg/domain

## 1. entity
Model to use on service. Naming your file based on context with suffix ```_model```.go or ```_entity```.go.

*example:*
```go
package entity

type DataResponse struct {
	ID   int64
	Name string
}
```

## 2. repository
Repository to use on usecase. Naming your file based on context with suffix ```_repository```.go.

*example:*
```go
package repository

type Repository interface {
	FunctionName(interface{}) (interface{}, error)
}
```