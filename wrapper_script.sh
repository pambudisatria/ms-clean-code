#!/bin/bash

#first process
./main &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start app: $status"
  exit $status
fi

#second process
fluentd -c ../conf/fluent.conf
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start fluentd: $status"
  exit $status
fi
